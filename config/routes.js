const express = require('express');
const router = express.Router();

const userController = require('../controllers/userController');
const logicController = require('../controllers/logicController');
const reportController = require('../controllers/reportController');

const auth = require('../middleware/auth');
const { ReadOnlyAnalyticsUserPropertyConfig } = require('facebook-nodejs-business-sdk');

router.get('/get_google_login_link', userController.get_google_login_link);
router.get('/google_auth_callback', userController.google_auth_callback);
router.get('/auth/user', auth, userController.getUserInfo);
router.post('/auth/login', userController.login);
router.post('/auth/signup', userController.signup);
router.post('/auth/forgot_password_request', userController.forgotPasswordRequest);
router.post('/auth/reset_password', userController.resetPassword);
router.post('/auth/resend_verify_email', userController.resendVerifyEmail);
router.post('/auth/verify_email', userController.verifyEmail);
router.post('/user/connectFBAccount', auth, userController.connectFBAccount);
router.post('/user/saveConnectedAdAccounts', auth, userController.saveConnectedAdAccounts);
router.post('/user/save_userinfo', auth, userController.saveUserInfo);

router.get('/live_ads', auth, logicController.getLiveAds);
router.get('/live_creative', auth, logicController.getLiveAdCreative);
router.get('/ad_image', auth, logicController.getAdImage);
router.get('/ad_video', auth, logicController.getAdVideo);
router.post('/ad_image', auth, logicController.postAdImage);
router.post('/ad', logicController.postAd);

router.post('/get_ad_report', auth, reportController.getReportPDF);

module.exports = router;