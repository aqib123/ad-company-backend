const port = process.env.PORT;
const baseURL = `http://localhost:${port}`;

console.log(process.env.PORT);

module.exports = {
    port: port,
    USE_HTTPS: false,
    TOKEN_SECRET: process.env.TOKEN_SECRET,
    oauth2Credentials: {
        client_id: process.env.GOOGLE_CLIENT_ID,
        project_id: process.env.GOOGLE_PROJECT_ID, // The name of your project
        api_key: process.env.GOOGLE_API_KEY,
        auth_uri: "https://accounts.google.com/o/oauth2/auth",
        token_uri: "https://oauth2.googleapis.com/token",
        auth_provider_x509_cert_url: "https://www.googleapis.com/oauth2/v1/certs",
        client_secret: process.env.GOOGLE_CLIENT_SECRET,
        redirect_uris: [
            `${baseURL}/auth_callback`
        ],
        scopes: [
            'https://www.googleapis.com/auth/plus.me',
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/userinfo.profile'
        ]
    },
    FACEBOOK_API: {
        APP_ID: process.env.FB_APP_ID,
        APP_SECRET: process.env.FB_APP_SECRET,
    }
};