'use strict';

var mysql = require('mysql');

var pool      =    mysql.createPool({
    connectionLimit : process.env.MYSQL_CONNECTION_LIMIT,
    waitForConnections : true,
    queueLimit :0,
    host     : process.env.MYSQL_HOST,
    user     : process.env.MYSQL_USER,
    password : process.env.MYSQL_PASSWORD,
    database : process.env.MYSQL_DATABASE,
    debug    :  false,
    wait_timeout : process.env.MYSQL_WAIT_TIMEOUT,
    connect_timeout : process.env.MYSQL_CONNECT_TIMEOUT,
    timezone: process.env.MYSQL_TIMEZONE
});

module.exports = pool;
