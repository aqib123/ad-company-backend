var pool = require('../config/database')
const bcrypt = require('bcryptjs');

exports.getUserInfo = function(id, callback) {
    try {
        pool.query("SELECT * FROM ac_user WHERE id = ?", id, (err, result) => {
            if (err) {
                callback(err, null)
            } else {
                if (result.length == 0) {
                    callback("Invalid credentials", null)
                } else {
                    callback(null, result[0]);
                }
            }
        })
    } catch (e) {
        callback(e, null)
    }

}

exports.userLogin = function(body, callback) {
    try {
        const { full_name, email, password, fb_id, fb_avatar, gg_id, auth_type } = body

        pool.query("SELECT * FROM ac_user WHERE email = ?", email, (err, result) => {
            if (err) {
                callback(err, null)
            } else {
                if (result.length == 0) {
                    if (auth_type === 0) { //email login
                        callback("Invalid credentials", null)
                    } else {
                        pool.query("INSERT INTO ac_user (full_name, email, password, fb_id, fb_avatar, gg_id, verify_token) VALUES ?", [[[full_name, email, '', fb_id, fb_avatar, gg_id, '']]], (err, result) => {
                            if (err) 
                                callback(err, null)
                            else
                                callback(null, {id: result.insertId, full_name, email, fb_avatar, verify_token: ''});
                        })
                    }
                } else {
                    if (auth_type === 0) { //email login
                        bcrypt.compare(password, result[0].password).then(isMatch => {
                            if (!isMatch) {
                                callback("Invalid credentials", null)
                            } else {
                                callback(null, result[0]);
                            }
                        });
                    } else { //social login
                        callback(null, result[0]);
                    }
                }
            }
        })
    } catch (e) {
        callback(e, null)
    }
}

exports.getConnectedAdManagers = function(user_id, callback) {
    try {
        pool.query("SELECT id, user_id, fb_ad_manager_id, name, currency, currency_symbol FROM ac_ad_manager WHERE user_id = ?", user_id, (err, result) => {
            if (err) {
                callback(err, null)
            } else {
                callback(null, result);
            }
        })
    } catch (e) {
        callback(e, null)
    }
}

exports.signup = function(body, callback) {
    try {
        const { full_name, email, password, auth_type, token } = body

        pool.query("SELECT * FROM ac_user WHERE email = ?", email, (err, result) => {
            if (err) {
                callback(err, null)
            } else {
                if (result.length == 0) {
                    bcrypt.genSalt(10, (err, salt) => {
                        bcrypt.hash(password, salt, (err, hash) => {
                            if (err) throw err;
                            pool.query("INSERT INTO ac_user (full_name, email, password, verify_token) VALUES ?", [[[full_name, email, hash, token]]], (err, result) => {
                                if (err) callback(err, null)
                                callback(null, result.insertId);
                            })
                        });
                    });
                } else {
                    callback("User already exist", null)
                }
            }
        })
    } catch (e) {
        callback(e, null)
    }
}

exports.updateUser = (body, callback) => {
    const { first_name, last_name, user_name, country, state, country_code, phonenumber, birthday, user_id } = body

    pool.query("UPDATE ac_user SET first_name = ?, last_name = ?, user_name = ?, country = ?, state = ?, country_code = ?, phonenumber=?, birthday=? WHERE id = ?", 
                [first_name, last_name, user_name, country, state, country_code, phonenumber, birthday, user_id], 
                (err, result) => {
                    if (err) {
                        callback(err, null)
                    }
                    callback(null, null);
    })
}

exports.addUserInquery = function(body, callback) {
    try {
        const { user_id, text } = body
        const now = new Date()

        pool.query("INSERT INTO user_inquiry (user_id, text, timestamp) VALUES ?", [[[user_id, text, now]]], (err, result) => {
            if (err) callback(err, null)
            callback(null, result.insertId)
        })
    } catch (e) {
        callback(e, null)
    }
}

exports.setForgotPasswordToken = function(params, callback) {
    const {email, token} = params
    try {
        pool.query("SELECT * FROM ac_user WHERE email = ?", email, (err, users) => {
            if (err) {
                callback(err, null)
            } else {
                if (users.length == 0) {
                    callback("Invalid credentials", null)
                } else {
                    pool.query("UPDATE ac_user SET forgot_password_token = ? WHERE id = ?", [token, users[0].id], (err, result) => {
                        if (err) callback(err, null)
                        callback(null, users[0]);
                    })
                }
            }
        })
    } catch (e) {
        callback(e, null)
    }
}

exports.resetPassword = function(params, callback) {
    const {token, password} = params
    try {
        pool.query("SELECT * FROM ac_user WHERE forgot_password_token = ?", token, (err, users) => {
            if (err) {
                callback(err, null)
            } else {
                if (users.length == 0) {
                    callback("Invalid token", null)
                } else {
                    bcrypt.genSalt(10, (err, salt) => {
                        bcrypt.hash(password, salt, (err, hash) => {
                            if (err) throw err;
                            pool.query("UPDATE ac_user SET password = ?, forgot_password_token = '' WHERE id = ?", [hash, users[0].id], (err, result) => {
                                if (err) callback(err, null)
                                callback(null, users[0]);
                            })
                        });
                    });
                }
            }
        })
    } catch (e) {
        callback(e, null)
    }
}

exports.setResetVerifyToken = function(params, callback) {
    const {email, token} = params
    try {
        pool.query("SELECT * FROM ac_user WHERE email = ?", email, (err, users) => {
            if (err) {
                callback(err, null)
            } else {
                if (users.length == 0) {
                    callback("Invalid credentials", null)
                } else {
                    pool.query("UPDATE ac_user SET verify_token = ? WHERE id = ?", [token, users[0].id], (err, result) => {
                        if (err) callback(err, null)
                        callback(null, users[0]);
                    })
                }
            }
        })
    } catch (e) {
        callback(e, null)
    }
}

exports.verifyEmail = function(token, callback) {
    try {
        pool.query("SELECT * FROM ac_user WHERE verify_token = ?", token, (err, users) => {
            if (err) {
                callback(err, null)
            } else {
                if (users.length == 0) {
                    callback("Invalid token", null)
                } else {
                    pool.query("UPDATE ac_user SET verify_token = '' WHERE id = ?", [users[0].id], (err, result) => {
                        if (err) callback(err, null)
                        callback(null, users[0]);
                    })
                }
            }
        })
    } catch (e) {
        callback(e, null)
    }
}

exports.connectFBAccount = function(body, callback) {
    try {
        const { id, full_name, email, password, fb_id, fb_avatar } = body

        pool.query("SELECT * FROM ac_user WHERE id = ?", id, (err, result) => {
            if (err) {
                callback(err, null)
            } else {
                if (result.length == 0) {
                    callback("Invalid credentials", null)
                } else {
                    console.log(fb_avatar)
                    pool.query("UPDATE ac_user SET fb_id = ?, fb_avatar = ? WHERE id = ?", [fb_id, fb_avatar, id], (err, result) => {
                        if (err) callback(err, null)
                        else callback(null, '');
                    })
                }
            }
        })
    } catch (e) {
        callback(e, null)
    }
}

exports.saveConnectedAdAccounts = function(body, callback) {
    try {
        const { id, ad_account_list } = body

        console.log(ad_account_list);

        pool.query("DELETE FROM ac_ad_manager WHERE user_id = ?", id, (err, result) => {
            if (err) {
                callback(err, null)
            } else {
                if (ad_account_list.length > 0) {
                    let sql = "INSERT INTO ac_ad_manager (user_id, fb_ad_manager_id, name, currency, currency_symbol) VALUES";
                
                    for (let i = 0; i < ad_account_list.length; i++) {
                        sql += `${i == 0 ? '' : ','} (${id}, '${ad_account_list[i]['id']}', '${ad_account_list[i]['name']}', '${ad_account_list[i]['currency']}', '${ad_account_list[i]['currency_symbol']}')`;
                    }
    
                    console.log(sql);
    
                    pool.query(sql, (err, result) => {
                        if (err) callback(err, null)
                            callback(null, '');
                    })
                } else {
                    callback(null, '');
                }
            }
        })
    } catch (e) {
        callback(e, null)
    }
}