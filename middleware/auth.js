const jwt = require('jsonwebtoken');

// eslint-disable-next-line consistent-return
module.exports = async function(req, res, next) {
  // Get token from header
  const token = req.header('x-auth-token');

  // Check if not token
  if (!token) {
    return res.json({
      success: false,
      message: 'No token, authorization denied'
    });
  }
  
  // Verify token
  try {
    const { id, is_admin, full_name, email, fb_id, fb_avatar, emailConfirmed, isConnectedAdManager, connectedAdManagerList, first_name, last_name, user_name, country_code, country, state, phonenumber, birthday } = jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = { id, is_admin, full_name, email, fb_id, fb_avatar, emailConfirmed, isConnectedAdManager, connectedAdManagerList, first_name, last_name, user_name, country_code, country, state, phonenumber, birthday };
    next();
  } catch (err) {
    res.json({ success: false, message: 'Token is not valid' });
  }
};
