const request = require("request");

const template_id_list = {
    verify_email:   'd-c55b627cd78543dfb3386deefef43f03',
    password_reset: 'd-0f6efd7d8a1e4531ad10356c91f1dc45',
};

const sendEmail = (to, content, subject, template_id) => {
    const options = { 
        method: 'POST',
        url: process.env.SENDGRID_API_URL,
        headers: { 
            'content-type': 'application/json',
            authorization: 'Bearer ' + process.env.SENDGRID_API_KEY
        },
        body: {
            personalizations: [{ 
                to: to,
                dynamic_template_data: content,
                subject: subject
            }],
            from: { email: 'no-reply@adcomply.io', name: 'AdComply' },
            reply_to: { email: 'no-reply@adcomply.io', name: 'AdComply' },
            template_id: template_id
        },
        json: true 
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);
        console.log("SendGrid: ", error, body);
    });
}


module.exports.sendVerifyEmail = (email, name, token) => {
    sendEmail(
        [
            {email, name},
        ],
        {
            name,
            username: name, 
            action_url: process.env.BASE_URL + '/verify/' + token
        }, 
        'Verify Email',
        template_id_list.verify_email
    );
};

module.exports.sendResetPasswordEmail = (email, name, token) => {
    sendEmail(
        [
            {email, name},
        ],
        {
            name,
            username: name, 
            action_url: process.env.BASE_URL + '/reset_password/' + token
        }, 
        'Reset Password',
        template_id_list.password_reset
    );
};

