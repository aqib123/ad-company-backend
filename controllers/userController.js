const jwt = require('jsonwebtoken')
const settings = require('../config/settings')
const google = require('googleapis').google
const userModel = require('../models/user')
const crypto = require('crypto')
const sendgrid = require('../helper/sendgrid')

// Google's OAuth2 client
const OAuth2 = google.auth.OAuth2;

exports.get_google_login_link = function (req, res) {
  try {
    const oauth2Client = new OAuth2(settings.oauth2Credentials.client_id, settings.oauth2Credentials.client_secret, settings.oauth2Credentials.redirect_uris[0]);

    const loginLink = oauth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: settings.oauth2Credentials.scopes
    });
    // return res.render("ejs/index", { loginLink: loginLink });
    return res.status(200).json({
      message : 'success',
      data    : { loginLink }
    })
  } catch (e) {
    return res.status(400).json({
      message : e,
      data    : null
    })
  }
}

exports.google_auth_callback = async function (req, res) {
  try {
    // Create an OAuth2 client object from the credentials in our config file
    const oauth2Client = new OAuth2(settings.oauth2Credentials.client_id, settings.oauth2Credentials.client_secret, settings.oauth2Credentials.redirect_uris[0]);

    if (req.query.error) {
      // The user did not give us permission.
      res.redirect('/glogin');
    } else {
      const data = await oauth2Client.getToken(req.query.code);
      const tokens = data.tokens;
      oauth2Client.setCredentials(tokens);
      const service = google.people({ version: 'v1', auth: oauth2Client });
      const me = await service.people.get({
        resourceName: 'people/me',
        personFields: 'names,emailAddresses',
      });
      const google_id = '';
      const name = me.data.names[0].displayName;
      const email = me.data.emailAddresses[0].value;
      
      userModel.userLogin({google_id, name, email}, (err, result) => {
        if (err) {
          res.status(400).json({
            message : err,
            data    : null
          })
        } else {
          tokens.user_id = result;
          res.cookie('jwt', jwt.sign(tokens, settings.TOKEN_SECRET));
          res.status(200).json({
            message : 'success',
            data    : { name, email }
          })
        }
      })
    }
  } catch (e) {
    res.status(400).json({
      message : e,
      data    : null
    })
    // res.redirect('/glogin');
  }
}

exports.getUserInfo = function (req, res, next) {
  try {
    userModel.getUserInfo(req.user.id, function(err, user) {
      if (err)
        res.status(400).json({message:err, data: null})
      else {
        const emailConfirmed = (user.verify_token === '' || user.verify_token === null) ? true : false
        userModel.getConnectedAdManagers(user.id, function(err, adManagerList) {
          const isConnectedAdManager = adManagerList.length === 0 ? false : true
          res.status(200).json({
            success: true,
            user: { 
              id: user.id, 
              is_admin: user.is_admin, 
              full_name: user.full_name, 
              first_name: user.first_name,
              last_name: user.last_name,
              user_name: user.user_name,
              country_code: user.country_code,
              country: user.country,
              state: user.state,
              phonenumber: user.phonenumber,
              birthday: user.birthday,
              email: user.email,
              fb_id: user.fb_id, 
              fb_avatar: user.fb_avatar, 
              emailConfirmed, 
              isConnectedAdManager, 
              connectedAdManagerList: adManagerList
            },
          });
        })
      }
    })
  } catch (e) {
    res.status(400).json({message: e})
  }
}

exports.login = function (req, res, next) {
  try {
    const { full_name, email, password, fb_id, fb_avatar, gg_id, auth_type } = req.body;
    console.log(req.body)
    //  Simple validation
    if ((auth_type === 0 && (!email || !password))  //email login
      || (auth_type === 1 && !fb_id)              //facebook login
      || (auth_type === 2 && !gg_id)) {           //google login
      return res.json({
          success: false,
          error: 'Please enter all fields'
      });
    }
  
    userModel.userLogin({full_name, email, password, fb_id, fb_avatar, gg_id, auth_type}, function(err, user) {
      if (err)
        res.json({message:err, data: null})
      else {
        if (user.verify_token && user.verify_token !== '') {
          res.status(200).json({success: false, verified: false})
        } else {
          const emailConfirmed = (user.verify_token === '' || user.verify_token === null) ? true : false
          userModel.getConnectedAdManagers(user.id, function(err, adManagerList) {
            const isConnectedAdManager = adManagerList.length === 0 ? false : true
            jwt.sign(
              { 
                id: user.id,
                is_admin: user.is_admin, 
                full_name: user.full_name,
                first_name: user.first_name,
                last_name: user.last_name,
                user_name: user.user_name,
                country_code: user.country_code,
                country: user.country,
                state: user.state,
                phonenumber: user.phonenumber,
                birthday: user.birthday,
                email: user.email,
                fb_id: user.fb_id,
                fb_avatar: user.fb_avatar,
                emailConfirmed,
                isConnectedAdManager,
                connectedAdManagerList: adManagerList
              },
              process.env.TOKEN_SECRET,
              { expiresIn: '7d' },
              (err, token) => {
                res.json({
                  success: true,
                  message: 'User logged in',
                  user: {
                    id: user.id,
                    is_admin: user.is_admin, 
                    full_name: user.full_name,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    user_name: user.user_name,
                    country_code: user.country_code,
                    country: user.country,
                    state: user.state,
                    phonenumber: user.phonenumber,
                    birthday: user.birthday,
                    email: user.email,
                    fb_id: user.fb_id,
                    fb_avatar: user.fb_avatar,
                    emailConfirmed,
                    isConnectedAdManager,
                    connectedAdManagerList: adManagerList
                  },
                  token,
                });
              }
            );  
          })
        }
      }
    });
  } catch (e) {
    res.json({message: e})
  }
}

exports.signup = function (req, res, next) {
  try {
    const { full_name, email, password } = req.body;
    //  Simple validation
    if (!email || !password) {
      return res.json({
        success: false,
        error: 'Please enter all fields'
      });
    }

    const token = crypto.randomBytes(64).toString('hex');
  
    userModel.signup({full_name, email, password, token}, function(err, result) {
      if (err)
        res.status(400).json({error:err, success: false})
      else {
        sendgrid.sendVerifyEmail(email, full_name, token);

        res.json({
          success: true,
          message: '',
        });
      }
    });
  } catch (e) {
    res.status(400).json({message: e})
  }
}

exports.saveUserInfo = (req, res, next) => {
  try {
    const { first_name, last_name, user_name, country, state, country_code, phonenumber, birthday } = req.body;

    userModel.updateUser({first_name, last_name, user_name, country, state, country_code, phonenumber, birthday, user_id: req.user.id}, function(err, result) {
      if (err)
        res.status(400).json({error:err, success: false})
      else {
        res.json({
          success: true,
          message: '',
        });
      }
    });
  } catch (e) {
    res.status(400).json({message: e})
  }
}

exports.forgotPasswordRequest = function (req, res, next) {
  try {
    const { email } = req.body;
    //  Simple validation
    if (!email) {
      return res.json({
        success: false,
        error: 'Invlid Params'
      });
    }

    const token = crypto.randomBytes(64).toString('hex');

    userModel.setForgotPasswordToken({email, token}, function(err, user) {
      if (err)
        res.status(400).json({error:err, success: false})
      else {
        sendgrid.sendResetPasswordEmail(email, user.full_name, token);
        res.json({
          success: true,
          message: '',
        });
      }
    });
  } catch (e) {
    res.status(400).json({success: false, error: e.message})
  }
}

exports.resetPassword = function (req, res, next) {
  try {
    const { token, password } = req.body;
    //  Simple validation
    if (!token || !password) {
      return res.json({
        success: false,
        error: 'Invalid Params'
      });
    }
  
    userModel.resetPassword({token, password}, function(err, result) {
      if (err)
        res.status(400).json({error:err, success: false})
      else {
        res.json({success: true, message: ''});
      }
    });
  } catch (e) {
    res.status(400).json({success: false, error: e.message})
  }
}

exports.resendVerifyEmail = function (req, res, next) {
  try {
    const { email } = req.body;
    //  Simple validation
    if (!email) {
      return res.json({
        success: false,
        error: 'Invlid Params'
      });
    }

    const token = crypto.randomBytes(64).toString('hex');

    userModel.setResetVerifyToken({email, token}, function(err, user) {
      if (err)
        res.status(400).json({error:err, success: false})
      else {
        sendgrid.sendVerifyEmail(email, user.full_name, token);
        res.json({
          success: true,
          message: '',
        });
      }
    });
  } catch (e) {
    res.status(400).json({success: false, error: e.message})
  }
}

exports.verifyEmail = function (req, res, next) {
  try {
    const { token } = req.body;

    if (!token) {
      return res.json({
          success: false,
          error: 'Please enter all fields'
      });
    }

    userModel.verifyEmail(token, function(err, user) {
      if (err)
        res.status(400).json({message:err, data: null})
      else {
        console.log(user)
        const emailConfirmed = true
        userModel.getConnectedAdManagers(user.id, function(err, adManagerList) {
          const isConnectedAdManager = adManagerList.length === 0 ? false : true
          jwt.sign(
            { id: user.id, is_admin: 0, full_name: user.full_name, email: user.email, fb_id: user.fb_id, fb_avatar: user.fb_avatar, emailConfirmed, isConnectedAdManager, connectedAdManagerList: adManagerList },
            process.env.TOKEN_SECRET,
            { expiresIn: '7d' },
            (err, token) => {
              res.json({
                success: true,
                message: 'User logged in',
                user: { id: user.id, is_admin: 0, full_name: user.full_name, email: user.email, fb_id: user.fb_id, fb_avatar: user.fb_avatar, emailConfirmed, isConnectedAdManager, connectedAdManagerList: adManagerList },
                token,
              });
            }
          );  
        })
      }
    });
  } catch (e) {
    res.status(400).json({message: e})
  }
}

exports.connectFBAccount = function(req, res, next) {
  try {
    const { full_name, email, fb_id, fb_avatar, access_token } = req.body;
  
    userModel.connectFBAccount({id: req.user.id, full_name, email, fb_id, fb_avatar}, function(err, user) {
      if (err)
        res.status(400).json({message:err, data: null})
      else {
        res.status(200).json({success:true, data: null})
      }
    });
  } catch (e) {
    res.status(400).json({message: e})
  }
}

exports.saveConnectedAdAccounts = function(req, res, next) {
  try {
    const { ad_account_list } = req.body;
  
    userModel.saveConnectedAdAccounts({id: req.user.id, ad_account_list}, function(err, user) {
      console.log(err, user);

      if (err)
        res.status(400).json({message:err, data: null})
      else {
        res.status(200).json({success:true, data: null})
      }
    });
  } catch (e) {
    res.status(400).json({message: e})
  }
}