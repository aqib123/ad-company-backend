const adsSdk = require('facebook-nodejs-business-sdk');
const AdAccount = adsSdk.AdAccount;
const Campaign = adsSdk.Campaign;
const Ad = adsSdk.Ad;
const AdSet = adsSdk.AdSet;
const AdCreative = adsSdk.AdCreative;
const AdImage = adsSdk.AdImage;
const AdVideo = adsSdk.AdVideo;

exports.getLiveAds = async (req, res) => {
    try {
        const accessToken = req.query.accessToken;
        const accountId = req.query.accountId;

        const api = adsSdk.FacebookAdsApi.init(accessToken);
        const account = new AdAccount(accountId);

        let fields = Object.keys(Ad.Fields).map((key) => Ad.Fields[key]);
        let creative_fields = Object.keys(AdCreative.Fields).map((key) => AdCreative.Fields[key]);

        const index = fields.indexOf('creative');
        fields[index] = `creative{${creative_fields.toString()}}`;
        let response = await account.getAds(fields, {effective_status: ['ACTIVE'], limit: 10});

        let ads = [...response];

        // console.log(ads.hasNext())

        try {
            while (response.hasNext()) {
                response = await response.next();
                ads = [...ads, ...response]
            }
        } catch (e) {console.log(e)}

        res.json({
            status: "success",
            ads
        })
    } catch (e) {
        console.log(e)
        res.json(e)
    }
};

exports.getLiveAdCreative = async (req, res) => {
    try {
        const accessToken = req.query.accessToken;
        const accountId = req.query.accountId;
        const adCreativeId = req.query.adCreativeId;

        const api = adsSdk.FacebookAdsApi.init(accessToken);
        const account = new AdAccount(accountId);

        let fields = Object.keys(AdCreative.Fields).map((key) => AdCreative.Fields[key]);

        let creative = new AdCreative(adCreativeId);
        creative = await creative.read(fields);

        res.json({
            status: "success",
            creative
        })
    } catch (e) {
        res.json({
            error: e
        })
    }
}

exports.getAdImage = async (req, res) => {
    try {
        const accessToken = req.query.accessToken;
        const accountId = req.query.accountId;
        const imageHash = req.query.imageHash;

        const api = adsSdk.FacebookAdsApi.init(accessToken);
        const account = new AdAccount(accountId);

        const fields = Object.keys(AdImage.Fields).map((key) => AdImage.Fields[key]);

        const adImages = await account.getAdImages(fields, { hashes: [imageHash] });

        res.json({
            status: "success",
            adImage: adImages.length > 0 ? adImages[0] : {}
        })
    } catch (e) {
        res.json({
            error: e
        })
    }
}

exports.getAdVideo = async (req, res) => {
    try {
        const accessToken = req.query.accessToken;
        const accountId = req.query.accountId;
        const adVideoId = req.query.adVideoId;        

        const api = adsSdk.FacebookAdsApi.init(accessToken);
        const account = new AdAccount(accountId);

        const fields = Object.keys(AdVideo.Fields).map((key) => AdVideo.Fields[key]);

        // const adVideos = await account.getAdVideos(fields, { hashes: [imageHash] });
        let adVideo = new AdVideo(adVideoId);
        adVideo = await adVideo.read([ 'id', 'source' ]);

        res.json({
            status: "success",
            adVideo
        })
    } catch (e) {
        res.json({
            error: e
        })
    }
}

exports.postAdImage = async (req, res) => {
    try {
        if (!req.files || Object.keys(req.files).length === 0) {
            return res.json({ status: 'failed', error: 'No files were uploaded.'});
        }
    
        const accessToken = req.body.accessToken;
        const accountId = req.body.accountId;
        // const accessToken = 'EAAJPjrbl8AwBAE3ZBKAgqqcZCZAHuPubCZA48Ls84h5TMRZBR5yOqWo6Ap0qzS2IHhXerL4zwk791k6715SBKvveOx5RRFl73kGYyZA1XCRxPpPzZCVS7cLJB01F4dv6nRUYQLDcGP1YhLEIJEIqZCgmTbKLCJDDWsq0p9C5UAELo6xgN8aQojLG5AsBnntRcwUSqhSYqrqi776VIYKYaZBuc';
        // const accountId = 'act_806743976540395';
        const api = adsSdk.FacebookAdsApi.init(accessToken);
        const account = new AdAccount(accountId);

        const file = req.files.file;

        console.log(file)

        const fields = Object.keys(AdImage.Fields).map((key) => AdImage.Fields[key]);

        // const data = fs.readFileSync('/Users/strivert/Documents/PCB/node.jpg', {encoding: 'base64'});
        const adImage = await account.createAdImage(fields, {
            // bytes: data
            bytes: file.data.toString('base64')
        });

        console.log(fields)

        res.json({
            status: "success",
            adImage,
            fields
        })
    } catch (e) {
        res.json({
            error: e
        })
    }
}

exports.postAd = async (req, res) => {
    try {
        // const campaign_fields = Object.keys(Campaign.Fields).map((key) => Campaign.Fields[key]);
        // const adset_fields = Object.keys(AdSet.Fields).map((key) => AdSet.Fields[key]);
        const ad_info = req.body.ad;
        const accessToken = req.body.accessToken;
        const accountId = req.body.accountId;

        const api = adsSdk.FacebookAdsApi.init(accessToken);
        const account = new AdAccount(accountId);

        const ad_fields = Object.keys(Ad.Fields).map((key) => Ad.Fields[key]);
        const ad_creative_fields = Object.keys(AdCreative.Fields).map((key) => AdCreative.Fields[key]);
        console.log(accessToken)
        console.log(accountId)

        if (ad_info.source_ad_id && ad_info.source_ad_id == "0") {
            delete ad_info.source_ad_id;
        }

        let response;
        // if (ad_info['id'] && ad_info['id'] != '') {
        //     response = await new Ad(ad_info.id, ad_info).update();
        // } else {
            response = await account.createAd([], ad_info);
        // }

        console.log(response);

        res.json({
            status: "success",
            // campaign_fields,
            // adset_fields,
            ad_fields,
            ad_creative_fields
        })
    } catch (e) {
        console.log(e.response.error)
        res.json({
            status: "failed",
            error_type: e.response && e.response.error ? e.response.error.type : 'Unknown Error',
            error: e.response && e.response.error ? e.response.error : {}
        })
    }
}

// "lead_campaign_id":       "120330000081640617"
// "link_click_campaign_id": "120330000081640817"
// "link_click_adsets_id":   "120330000081641117"
// "image_hash": "e8cdc331b2b75439d3e680d722719b30"