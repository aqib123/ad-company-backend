const adsSdk = require('facebook-nodejs-business-sdk');
const ejs = require('ejs');
const pdf = require('html-pdf');
const path = require('path');

const AdAccount = adsSdk.AdAccount;
const Ad = adsSdk.Ad;
const AdCreative = adsSdk.AdCreative;

exports.getReportPDF = async (req, res) => {
    const ad_info = req.body.ad;
    const accessToken = req.body.accessToken;
    const accountId = req.body.accountId;
    let isPassed = false;
    let error;

    try {
        const api = adsSdk.FacebookAdsApi.init(accessToken);
        const account = new AdAccount(accountId);

        const ad_fields = Object.keys(Ad.Fields).map((key) => Ad.Fields[key]);
        const ad_creative_fields = Object.keys(AdCreative.Fields).map((key) => AdCreative.Fields[key]);

        if (ad_info.source_ad_id && ad_info.source_ad_id == "0") {
            delete ad_info.source_ad_id;
        }

        response = await account.createAd([], ad_info);
        isPassed = true;
    } catch (e) {
        console.log(e.response.error)
        error = e.response && e.response.error ? e.response.error.error_user_msg : 'Unknown Error';
    }

    console.log(isPassed, error)

    ejs.renderFile('ejs/report-template.ejs', {isPassed, error, ad_info, username: req.user.full_name}, (err, data) => {
        if (err) {
              res.send(err);
        } else {
            let options = {
                "height": "11.25in",
                "width": "8.5in",
                "header": {
                    "height": "20mm"
                },
                "footer": {
                    "height": "20mm",
                },
            };
            pdf.create(data, options).toFile("pdfs/AdCheckReport.pdf", function (err, data) {
                if (err) {
                    res.send(err);
                } else {
                    res.download('pdfs/AdCheckReport.pdf');
                    // res.send("File created successfully");
                }
            });
        }
    });
}